# Download and install new version of git and clean up old versions

This script downloads and installs version 2.9.0 of git and removes any old versions.  It is heavily based on  [this script](https://gist.github.com/matthewriley/b74fa53594db1354e5593994c5d5b5a4 "this script") by Matt Wriley.  The script needs to be run with sudo permissions. 




